﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class ItemGenerator
{
    private GameObject level;
    private GameObject BlockBluePrefab;
    private GameObject BlockRedPrefab;
    private GameObject CoinBluePrefab;
    private GameObject CoinRedPrefab;
    private const float screenTop = 5f;

    public ItemGenerator(GameObject level, GameObject BlockBluePrefab, GameObject BlockRedPrefab, GameObject CoinBluePrefab, GameObject CoinRedPrefab)
    {
        this.level = level;
        this.BlockBluePrefab = BlockBluePrefab;
        this.BlockRedPrefab = BlockRedPrefab;
        this.CoinBluePrefab = CoinBluePrefab;
        this.CoinRedPrefab = CoinRedPrefab;
    }

    public void GenerateCoin(ObjectsColor color)
    {
        GameObject newGameObject = null;
        if (color == ObjectsColor.Blue)
        {
            newGameObject = GameObject.Instantiate(CoinBluePrefab);
        }
        else if (color == ObjectsColor.Red)
        {
            newGameObject = GameObject.Instantiate(CoinRedPrefab);
        }

        newGameObject.GetComponent<FallingObjects>().SetType(FallingObjectType.Coin);
        PositioningOfGameObject(newGameObject);
    }

    public void GenerateBlock(ObjectsColor color)
    {
        GameObject newGameObject = null;
        if (color == ObjectsColor.Blue)
        {
            newGameObject = GameObject.Instantiate(BlockBluePrefab);
        }
        else if (color == ObjectsColor.Red)
        {
            newGameObject = GameObject.Instantiate(BlockRedPrefab);
        }

        newGameObject.GetComponent<FallingObjects>().SetType(FallingObjectType.Block);
        PositioningOfGameObject(newGameObject);
    }

    private void PositioningOfGameObject(GameObject newGameObject)
    {
        int random = UnityEngine.Random.Range(0, 2);
        float xRandom;
        if (random == 0)
        {
            xRandom = newGameObject.transform.position.x;
        }
        else
        {
            xRandom = newGameObject.transform.position.x * 3;
        }

        Vector3 newPosition = new Vector3(xRandom, screenTop, 0);
        newGameObject.transform.position = newPosition;

        // put the object as a child of the GameObject level
        newGameObject.transform.SetParent(level.transform);
    }
}
