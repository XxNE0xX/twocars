﻿public enum FallingObjectType
{
    Block, Coin
}

public enum ObjectsColor
{
    Red, Blue
}