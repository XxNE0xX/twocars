﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridLayoutFixerByColumnCount : MonoBehaviour
{
    [SerializeField] int columnCount = 3;

    float oldY = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float cellX = this.GetComponent<RectTransform>().rect.width / columnCount;
        float cellY = this.GetComponent<RectTransform>().rect.height;

        if (oldY != cellY)
        {
            oldY = cellY;
            this.GetComponent<GridLayoutGroup>().cellSize = new Vector2(cellX, cellY);
        }
    }
}
