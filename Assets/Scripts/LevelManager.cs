﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    private float fallSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        fallSpeed = gameManager.GetWorldSpeed();
        UpdateSpeedOfChildren();
    }

    private void UpdateSpeedOfChildren()
    {
        FallingObjects[] fallingObjects = this.GetComponentsInChildren<FallingObjects>();
        foreach (FallingObjects stuff in fallingObjects)
        {
            stuff.SetFallSpeed(fallSpeed);
        }
    }

    public void BlockCollisionDetected()
    {
        gameManager.BlockGameOver();
    }

    public void CoinCollisionDetected()
    {
        gameManager.AddScore();
    }

    public void CoinMiss()
    {
        gameManager.CoinGameOver();
    }

    public void ResetLevel()
    {
        FallingObjects[] fallingObjects = this.GetComponentsInChildren<FallingObjects>();
        foreach (FallingObjects stuff in fallingObjects)
        {
            Destroy(stuff.gameObject);
        }
    }
}
