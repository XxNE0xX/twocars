﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private Car leftCar;
    [SerializeField] private Car rightCar;
    [SerializeField] private GameObject level;
    [SerializeField] private bool invincibility = false;
    [SerializeField] private InformationPanelManager informationPanel;
    [SerializeField] private GameObject gameOverSplashScreen;

    private float worldSpeed = 2f;

    private int score = 0;
    private const float speedIncreaseConstant = 0.4f;
    private const float timeIntervalDecreaseConstant = 0.2f;

    private string savedDataPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/TwoCarsData";
    private string savedScoreDataFileName = "/score.gdata";
    private int bestScore;

    private const string prefabsPath = "Prefabs/";
    private ItemGenerator itemGenerator;

    private int chanceOfItemGenrationPercent = 100;
    private float time = 0.0f;
    private float timeIntervalsForItemGeneration = 1.5f;

    // To make sure no two blocks spawn too close so the player can't pass them
    private float LastBlueBlockSpawnTime = 0.0f;
    private float LastRedBlockSpawnTime = 0.0f;
    private float safetyTimeForBlocks = 2f;

    private bool isGameOver = false;

    private void InitializeVariables()
    {
        worldSpeed = 2f;

        score = 0;

        chanceOfItemGenrationPercent = 100;
        time = 0.0f;
        timeIntervalsForItemGeneration = 1.5f;

        LastBlueBlockSpawnTime = 0.0f;
        LastRedBlockSpawnTime = 0.0f;

        isGameOver = false;
    }

    void Awake()
    {
        GameObject BlockBluePrefab = Resources.Load(prefabsPath + "BlockBlue") as GameObject;
        GameObject BlockRedPrefab = Resources.Load(prefabsPath + "BlockRed") as GameObject;
        GameObject CoinBluePrefab = Resources.Load(prefabsPath + "CoinBlue") as GameObject;
        GameObject CoinRedPrefab = Resources.Load(prefabsPath + "CoinRed") as GameObject;
        itemGenerator = new ItemGenerator(level, BlockBluePrefab, BlockRedPrefab, CoinBluePrefab, CoinRedPrefab);
    }

    void Start()
    {
        //itemGenerator.GenerateCoin(ObjectsColor.Blue);
        //itemGenerator.GenerateBlock(ObjectsColor.Red);
        LoadBestScore();
    }

    void Update()
    {
        CheckKeyboard();
        
        if (isGameOver == false)
        {
            time += Time.deltaTime;
            if (time >= timeIntervalsForItemGeneration)
            {
                time -= timeIntervalsForItemGeneration;
                GenerateItems();
            }
        }
    }

    private void CheckKeyboard()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            leftCar.ChangePosReq();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            rightCar.ChangePosReq();
        }
    }

    public void BlockGameOver()
    {
        GameOverGeneral();
    }

    public void CoinGameOver()
    {
        GameOverGeneral();
    }

    private void GameOverGeneral()
    {
        if (invincibility == false)
        {
            isGameOver = true;
            worldSpeed = 0;
            SaveBestScore();
            gameOverSplashScreen.SetActive(true);
        }
    }

    public void AddScore()
    {
        score++;
        informationPanel.UpdateScore(score);
        if (score > bestScore)
        {
            bestScore = score;
            informationPanel.UpdateBestScore(bestScore);
        }

        if (score % 5 == 0)
        {
            IncreaseDifficultyBySpeed();
        }
        if (score % 10 == 0)
        {
            IncreaseDifficultyByDecreaseSpawnDelay();
        }
    }

    private void IncreaseDifficultyBySpeed()
    {
        // We have added a random element to it so it doesn't feel linear
        worldSpeed += speedIncreaseConstant + (UnityEngine.Random.Range(-1, 2) * 0.1f);
    }

    private void IncreaseDifficultyByDecreaseSpawnDelay()
    {
    	if (timeIntervalsForItemGeneration > 0.5f)
    	{
        	// We have added a random element to it so it doesn't feel linear
        	timeIntervalsForItemGeneration -= timeIntervalDecreaseConstant + (UnityEngine.Random.Range(-1, 2) * 0.05f);
    	}
    }

    public float GetWorldSpeed()
    {
        return worldSpeed;
    }

    public bool IsGameOver()
    {
        return isGameOver;
    }

    private void GenerateItems()
    {
        int toGenerateChance = UnityEngine.Random.Range(0, 100);
        
        if (toGenerateChance <= chanceOfItemGenrationPercent)
        {
            int kindOfObject = UnityEngine.Random.Range(0, 2);
            int objectColor = UnityEngine.Random.Range(0, 2);
            if (kindOfObject == 0)
            {
                if (objectColor == 0)
                {
                    if (Time.realtimeSinceStartup - LastBlueBlockSpawnTime > safetyTimeForBlocks)
                    {
                        itemGenerator.GenerateBlock(ObjectsColor.Blue);
                        LastBlueBlockSpawnTime = Time.realtimeSinceStartup;
                    }
                }
                else
                {
                    if (Time.realtimeSinceStartup - LastBlueBlockSpawnTime > safetyTimeForBlocks)
                    {
                        itemGenerator.GenerateBlock(ObjectsColor.Red);
                        LastBlueBlockSpawnTime = Time.realtimeSinceStartup;
                    }
                }
            }
            else
            {
                if (objectColor == 0)
                {
                    itemGenerator.GenerateCoin(ObjectsColor.Blue);
                }
                else
                {
                    itemGenerator.GenerateCoin(ObjectsColor.Red);
                }
            }
        }
    }

    private void SaveBestScore()
    {
        Directory.CreateDirectory(savedDataPath);
        File.WriteAllText(savedDataPath + savedScoreDataFileName, bestScore.ToString());
    }

    private void LoadBestScore()
    {
        if (File.Exists(savedDataPath + savedScoreDataFileName))
        {
            StreamReader streamReader = new StreamReader(savedDataPath + savedScoreDataFileName);
            bestScore = Int32.Parse(streamReader.ReadLine());
            streamReader.Close();
        }
        else
        {
            Debug.Log("The Score File Doesn't Exist!");
            bestScore = 0;
        }
        informationPanel.UpdateBestScore(bestScore);
    }

    public void ReloadGame()
    {
        InitializeVariables();
        informationPanel.UpdateScore(score);
        LoadBestScore();

        level.GetComponent<LevelManager>().ResetLevel();

        leftCar.ResetCar();
        rightCar.ResetCar();

        gameOverSplashScreen.SetActive(false);
    }
}
