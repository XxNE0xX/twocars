﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{

    private float fallSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckPositionAndDestroy();
        Fall();
    }

    public void SetFallSpeed(float speed)
    {
        fallSpeed = speed;
    }

    private void Fall()
    {
        this.transform.position += new Vector3(0, 1, 0) * (fallSpeed * Time.deltaTime);
    }

    private void CheckPositionAndDestroy()
    {
        if (this.transform.position.y < -6)
        {
            Destroy(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider.gameObject);
        if (collider.GetComponent<Car>() != null)
        {
            this.GetComponentInParent<LevelManager>().BlockCollisionDetected();
        }
    }
}
