﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InformationPanelManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateScore(int score)
    {
        // Child zero is the score panel, and the first child of each information panel is the value
        this.transform.GetChild(0).transform.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(score.ToString());
    }

    public void UpdateBestScore(int bestScore)
    {
        // Child one is the best score panel, and the first child of each information panel is the value
        this.transform.GetChild(1).transform.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(bestScore.ToString());
    }
}
