﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridLayoutFixerByRowCount : MonoBehaviour
{
    [SerializeField] int rowCount = 2;

    float oldX = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float cellX = this.GetComponent<RectTransform>().rect.width;
        float cellY = this.GetComponent<RectTransform>().rect.height / rowCount;

        if (oldX != cellX)
        {
            oldX = cellX;
            this.GetComponent<GridLayoutGroup>().cellSize = new Vector2(cellX, cellY);
        }
    }
}
