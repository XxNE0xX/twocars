﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingObjects : MonoBehaviour
{
    private float fallSpeed;
    private FallingObjectType type;
    private const float screenBottom = -5f;
    private bool triggered = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckPositionAndDestroy();
        Fall();
    }

    public void SetFallSpeed(float speed)
    {
        fallSpeed = speed;
    }

    private void Fall()
    {
        this.transform.position += new Vector3(0, -1, 0) * (fallSpeed * Time.deltaTime);
    }

    private void CheckPositionAndDestroy()
    {
        if (type == FallingObjectType.Coin && this.transform.position.y < screenBottom + 0.5f)
        {
            this.GetComponentInParent<LevelManager>().CoinMiss();
        }
        if (this.transform.position.y < screenBottom - 2f)
        {
            Destroy(this.gameObject);
        }
    }

    public void SetType(FallingObjectType type)
    {
        this.type = type;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        triggered = true;
        if (type == FallingObjectType.Block)
        {
            this.GetComponentInParent<LevelManager>().BlockCollisionDetected();
        }
        else
        {
            // Destory should happen at last, otherwise the line of codes that come after would be unreachable
            this.GetComponentInParent<LevelManager>().CoinCollisionDetected();
            Destroy(this.gameObject);
        }
    }
}
