﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixInformationPanelSize : MonoBehaviour
{
    private float oldWidth = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float width = Screen.width / 5;
        if (width != oldWidth)
        {
            oldWidth = width;
            this.GetComponent<RectTransform>().sizeDelta = new Vector2 (width, 0);
        }
    }
}
