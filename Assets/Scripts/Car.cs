﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;

    private float changePositionSpeed = 50f;

    private float initialPos;
    private float otherPose;

    private bool changePosReq = false;
    private float originPos;
    private float destinationPos;
    private bool rotated = false;

    private bool isEngineOn = true;

    // it only becomes true when the car is in the middle of changing position and another request comes
    private bool changePosForReset = false;

    // Start is called before the first frame update
    void Start()
    {
        initialPos = this.transform.position.x;
        otherPose = initialPos * 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (changePosReq == true && gameManager.IsGameOver() == false)
        {
            ChangePos();
        }
        if (gameManager.IsGameOver() == true && isEngineOn == true)
        {
            ShutOffEngine();
        }
        if (changePosReq == false && changePosForReset == true)
        {
            changePosForReset = false;

            float currentPose = this.transform.position.x;
            if (currentPose.Equals(otherPose))
            {
                ChangePosReq();
            }
        }
    }

    public void ChangePosReq()
    {
        // Checking this condition is necessary to avoid multiple reqs exuting together that causes glitches in the game
        if (changePosReq == false)
        {
            float currentPos = this.transform.position.x;
            if (currentPos.Equals(initialPos))
            {
                destinationPos = otherPose;
                originPos = initialPos;
            }
            else
            {
                destinationPos = initialPos;
                originPos = otherPose;
            }
            changePosReq = true;
        }
    }

    private void ChangePos()
    {
        float currentPos = this.transform.position.x;
        if (rotated == false)
        {
            if (originPos < destinationPos)
            {
                this.transform.Rotate(0f, 0f, -45f, Space.Self);
            }
            else
            {
                this.transform.Rotate(0f, 0f, 45f, Space.Self);
            }
            rotated = true;
        }
        if (currentPos.Equals(destinationPos) == false)
        {
            this.transform.position += new Vector3(1, 0, 0) * (changePositionSpeed / (destinationPos - originPos)) * Time.deltaTime;
        }
        if (originPos < destinationPos && currentPos >= destinationPos)
        {
            this.transform.position = new Vector3(destinationPos, this.transform.position.y, this.transform.position.z);
            this.transform.Rotate(0f, 0f, 45f, Space.Self);

            rotated = false;
            changePosReq = false;
        }
        else if (originPos > destinationPos && currentPos <= destinationPos)
        {
            this.transform.position = new Vector3(destinationPos, this.transform.position.y, this.transform.position.z);
            this.transform.Rotate(0f, 0f, -45f, Space.Self);

            rotated = false;
            changePosReq = false;
        }
    }

    private void ShutOffEngine()
    {
        // We use this overload in case of the objects being deactivated
        this.GetComponentInChildren<ParticleSystem>(true).gameObject.SetActive(false);
        isEngineOn = false;
    }

    private void TurnOnEngine()
    {
        // We use this overload in case of the objects being deactivated
        this.GetComponentInChildren<ParticleSystem>(true).gameObject.SetActive(true);
        isEngineOn = true;
    }

    public void ResetCar()
    {
        float currentPose = this.transform.position.x;
        if (changePosReq == false) {
            if (currentPose.Equals(otherPose))
            {
                ChangePosReq();
            }
        }
        else
        {
            changePosForReset = true;
        }
        TurnOnEngine();
    }
}


